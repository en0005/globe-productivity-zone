﻿Shader "Custom/background" {
	Properties {
	    _Tint 	("Tint Color", Color) 		= (1, 1, 1, .1)
	    _Blend 	("Blend", Range(0.0,1.0)) 	= 0.0
	    _Tex 	("First", 2D) 				= "white" {}
	    _Tex2 	("Second", 2D) 				= "white" {}
	 
	}
	 
	SubShader {
	    Tags { "Queue" = "Transparent" }
	    Cull Back
	    Blend SrcAlpha OneMinusSrcAlpha
	    ZWrite Off
	    Lighting Off       
	    Color [_Tint]
	    Pass {
	        SetTexture [_Tex]  { combine texture }
	        SetTexture [_Tex2] { constantColor (0,0,0,[_Blend]) combine texture lerp(constant) previous }
	    }
	} 
	FallBack "Diffuse"
}
