﻿using UnityEngine;
using System.Collections;

public class scroll_v3 : MonoBehaviour {
	// Public Vars
	public Vector3 		carouselReferencePoint;
	public Transform 	background;
	public Texture[] 	backgroundArray = new Texture[8];
	public Texture[] 	baseArray 		= new Texture[8];
	public Texture[]	buttonTextures 	= new Texture[7];
	public string[]		playerDesc 		= new string[8];
	public string[]		trackNameArr	= new string[8];
	public string[]		artistNameArr	= new string[8];

	// Private Vars - Although the default access in C# is private, it is included for readability
	private bool 		rotateDone 			= true;
	private Direction 	spinDir;
	private float 		angleStep 			= 25.73f;
	private int 		currentPosition 	= 0;
	private int			iconsDoneRotating	= 0;
	private GameObject	backgroundObject;
	private GameObject 	baseObject;
	private GameObject	rectangleObject;
	private GameObject	infoObject;
	private GameObject	trackName;
	private GameObject	artistName;
	private ParticleSystem pSystem;
	
	private Vector2 	firstPos 			= new Vector2 (0, 0);
	private Vector2 	lastPos 			= new Vector2 (0, 0);
	private float 		swipeThreshold 		= 0.5f;
	private bool		isSwiping			= false;
	private float 		startTime;
	private float		endTime;
	private float 		finalPos;

	private Rect		playButton;
	private Rect		pauseButton;
	private Rect		stopButton;

	// Enumerations
	public enum Direction 	{left, right};

	void Start() {
		backgroundObject = GameObject.Find ("background");
		baseObject 		 = GameObject.Find ("base");
		rectangleObject	 = GameObject.Find ("rectangle");
		infoObject	 	 = GameObject.Find ("info");
		trackName	 	 = GameObject.Find ("track_name");
		artistName	 	 = GameObject.Find ("artist_name");

		rectangleObject.renderer.material.SetColor 	("_Color", new Color (0, 0, 0, 0));
		infoObject.renderer.material.SetColor 		("_Color", new Color (1, 1, 1, 0));
		trackName.renderer.material.SetColor 		("_Color", new Color (1, 1, 1, 0));
		artistName.renderer.material.SetColor 		("_Color", new Color (1, 1, 1, 0));

		TextMesh infoText 	= GameObject.Find ("info").GetComponent<TextMesh> ();
		pSystem 			= GameObject.Find ("emitter").GetComponent<ParticleSystem> ();

		setupPlayerDescription ();
	}

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			firstPos.x 	= Input.mousePosition.x;
			firstPos.y 	= Input.mousePosition.y;
			startTime 	= Time.time; // Start timer on mouse down
		} else if (Input.GetMouseButtonUp (0)) {
			lastPos.x 	= Input.mousePosition.x;
			lastPos.y 	= Input.mousePosition.y;
			endTime 	= Time.time; // Save current timer 

			checkClick();
			checkSwipe();
		}
	}

	void checkClick() {
		RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		
		if (hit.collider != null && hit.transform != null) {
			print (hit.collider.name);
		}
	}
	
	void checkSwipe() {
		if(endTime - startTime < swipeThreshold && isSwiping == false) {
			if (firstPos.x - lastPos.x > 0 && currentPosition < 6) { // Swipe left
				rotateCarousel(Direction.right);
			}
			
			if (firstPos.x - lastPos.x < 0 && currentPosition >= 1) { // Swipe right
				rotateCarousel(Direction.left);
			}
		}
		
		startTime = 0; // Reset timer on mouse up
	}
	
	void OnGUI() {
		GUI.backgroundColor = new Color(0, 0, 0, 0);
		
		GUI.Label (new Rect (155, 62.5f, 400, 100), "<size=20>Discover how music became portable</size>");

		int buttonSize = 0;
		if(Screen.height > Screen.width) {
			buttonSize = Mathf.RoundToInt(Screen.height * .06f);
		} else if (Screen.width > Screen.height) {
			buttonSize = Mathf.RoundToInt(Screen.width * .06f);
		}

		playButton = new Rect (Screen.width * 0.20f, Screen.height * 0.84f, buttonSize, buttonSize);

		if (GUI.Button(new Rect(10, Screen.height * 0.55f, buttonSize, buttonSize), buttonTextures[0]) && currentPosition > 0 && this.rotateDone == true) {
			rotateCarousel(Direction.left);
		} else if (GUI.Button(new Rect(Screen.width - (buttonSize + 10), Screen.height * 0.55f, buttonSize, buttonSize), buttonTextures[1]) && currentPosition < 6 && this.rotateDone == true) {
			rotateCarousel(Direction.right);
		} else if (GUI.Button(new Rect(Screen.width * 0.05f, Screen.height * 0.79f, buttonSize, buttonSize), buttonTextures[2]) && infoObject.renderer.material.color.a == 0) {
			StartCoroutine("showRectangle");
			StartCoroutine("showInfo");
		} else if (GUI.Button(new Rect(Screen.width * 0.05f, Screen.height * 0.89f, buttonSize, buttonSize), buttonTextures[3]) && trackName.renderer.material.color.a == 0) {
			StartCoroutine("showRectangle");
			StartCoroutine("showPlayer");
		}
	}

	void rotateCarousel(Direction passedDir) {
		this.spinDir 	= passedDir;
		this.isSwiping 	= true;

		if(passedDir == Direction.left) {
			currentPosition -= 1;
			transform.Rotate (Vector3.down * angleStep);
			this.rotateDone = false;
		} else if(passedDir == Direction.right) {
			currentPosition += 1;
			transform.Rotate (Vector3.up * angleStep);
			this.rotateDone = false;
		}

		StartCoroutine ("switchBackground");
		StartCoroutine ("switchBase");
		StartCoroutine ("hideInfo");
		StartCoroutine ("hidePlayer");
		StartCoroutine ("hideRectangle");
		for(int i = 0; i < Component.FindObjectsOfType<carousel_icon>().Length; i++) {
			Component.FindObjectsOfType<carousel_icon>()[i].carouselRotated(this.spinDir);
		}
	}

	IEnumerator switchBackground() {
		backgroundObject.renderer.material.SetTexture ("_Tex2", backgroundArray[currentPosition]);
		float newBlendValue;
		// Blend icon materials
		while(backgroundObject.renderer.material.GetFloat("_Blend") != 1f) {
			newBlendValue = Mathf.Lerp (backgroundObject.renderer.material.GetFloat("_Blend"), 1f, 0.1f + Time.deltaTime);
			backgroundObject.renderer.material.SetFloat ("_Blend", newBlendValue);

			if(newBlendValue >= 0.9f) {
				backgroundObject.renderer.material.SetFloat ("_Blend", 1f);
				break;
			}

			yield return null;
		}

		backgroundObject.renderer.material.SetTexture ("_Tex", backgroundArray[currentPosition]);
		backgroundObject.renderer.material.SetFloat ("_Blend", 0f);
		yield return null;
	}

	IEnumerator switchBase() {
		baseObject.renderer.material.SetTexture ("_Tex2", baseArray[currentPosition]);
		float newBlendValue;
		// Blend icon materials
		while(baseObject.renderer.material.GetFloat("_Blend") != 1f) {
			newBlendValue = Mathf.Lerp (baseObject.renderer.material.GetFloat("_Blend"), 1f, 0.1f + Time.deltaTime);
			baseObject.renderer.material.SetFloat ("_Blend", newBlendValue);
			
			if(newBlendValue >= 0.9f) {
				baseObject.renderer.material.SetFloat ("_Blend", 1f);
				break;
			}
			
			yield return null;
		}
		
		baseObject.renderer.material.SetTexture ("_Tex", baseArray[currentPosition]);
		baseObject.renderer.material.SetFloat ("_Blend", 0f);
		yield return null;
	}

	IEnumerator showRectangle() {
		pSystem.Play ();
		infoObject.GetComponent<TextMesh> ().text = playerDesc[currentPosition];

		float newBlendValue;
		// Change alpha
		while(rectangleObject.renderer.material.GetFloat("_Blend") != 1f) {
			newBlendValue = Mathf.Lerp (rectangleObject.renderer.material.GetFloat("_Blend"), 1f, 0.01f + Time.deltaTime);
			rectangleObject.renderer.material.SetFloat ("_Blend", newBlendValue);
			
			if(newBlendValue >= 0.9f) {
				rectangleObject.renderer.material.SetFloat ("_Blend", 1f);
				break;
			}
			
			yield return null;
		}
		
		yield return null;
	}

	IEnumerator hideRectangle() {
		float newBlendValue;
		// Change alpha
		while(rectangleObject.renderer.material.GetFloat("_Blend") != 0f) {
			newBlendValue = Mathf.Lerp (rectangleObject.renderer.material.GetFloat("_Blend"), 0f, 0.02f + Time.deltaTime);
			rectangleObject.renderer.material.SetFloat ("_Blend", newBlendValue);
			
			if(newBlendValue >= 0.1f) {
				rectangleObject.renderer.material.SetFloat ("_Blend", 0f);
				break;
			}
			
			yield return null;
		}
		
		yield return null;
	}

	IEnumerator showInfo() {
		StartCoroutine ("hidePlayer");
		float newBlendValue;
		// Change alpha
		while(infoObject.renderer.material.GetColor("_Color").a != 1f) {
			newBlendValue = Mathf.Lerp (infoObject.renderer.material.GetColor("_Color").a, 1f, 0.01f + Time.deltaTime);
			infoObject.renderer.material.SetColor ("_Color", new Color(1, 1, 1, newBlendValue));
			
			if(newBlendValue >= 0.9f) {
				infoObject.renderer.material.SetColor ("_Color", new Color(1, 1, 1, 1));
				break;
			}
			
			yield return null;
		}

		yield return null;
	}

	IEnumerator hideInfo() {
		float newBlendValue;
		// Change alpha
		while(infoObject.renderer.material.GetColor("_Color").a != 0f) {
			newBlendValue = Mathf.Lerp (infoObject.renderer.material.GetColor("_Color").a, 0f, 0.02f + Time.deltaTime);
			infoObject.renderer.material.SetColor ("_Color", new Color(1, 1, 1, newBlendValue));
			
			if(newBlendValue >= 0.1f) {
				infoObject.renderer.material.SetColor ("_Color", new Color(1, 1, 1, 0));
				break;
			}
			
			yield return null;
		}
		
		yield return null;
	}

	IEnumerator showPlayer() { // CHANGE OBJECT
		StartCoroutine ("hideInfo");
		trackName.GetComponent<TextMesh> ().text = trackNameArr [currentPosition];
		artistName.GetComponent<TextMesh> ().text = artistNameArr [currentPosition];
		float newBlendValue;
		// Change alpha
		while(trackName.renderer.material.GetColor("_Color").a != 1f) {
			newBlendValue = Mathf.Lerp (trackName.renderer.material.GetColor("_Color").a, 1f, 0.01f + Time.deltaTime);
			trackName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, newBlendValue));
			artistName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, newBlendValue));
			
			if(newBlendValue >= 0.9f) {
				trackName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, 1));
				artistName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, 1));
				break;
			}
			
			yield return null;
		}
		
		yield return null;
	}
	
	IEnumerator hidePlayer() { // CHANGE OBJECT
		float newBlendValue;
		// Change alpha
		while(trackName.renderer.material.GetColor("_Color").a != 0f) {
			newBlendValue = Mathf.Lerp (trackName.renderer.material.GetColor("_Color").a, 0f, 0.02f + Time.deltaTime);
			trackName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, newBlendValue));
			artistName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, newBlendValue));
			
			if(newBlendValue >= 0.1f) {
				trackName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, 0));
				artistName.renderer.material.SetColor ("_Color", new Color(1, 1, 1, 0));
				break;
			}
			
			yield return null;
		}
		
		yield return null;
	}

	private void setupPlayerDescription() {
		playerDesc [0] = "<size=38>Play it yourself, or hire a traveling band</size>" +
						"\nAnyone with the right tools could play anything and call it \"music\". Paper is very portable. Instruments? Not so.";
		
		playerDesc [1] = "<size=38>Wind-up Chronicles</size>" +
			"\nThe rich ladies and sirs of this period carried tiny version of their favorite songs in their pockets.";

		playerDesc [2] = "<size=38>Don't change that dial!</size>" +
			"\nTune in to your favorite music. That is, if the DJ is playing it.";
		
		playerDesc [3] = "<size=38>Mixtape Madness</size>" +
			"\nFinally, something worthy of the term \"portable\"!";
		
		playerDesc [4] = "<size=38>The Roman Empire</size>" +
			"\nCDs are like cassettes, except they sound better, and you can skip through tracks.";
		
		playerDesc [5] = "<size=38>Status: Single</size>" +
			"\nWith Mp3 players, hold your entire collection in your hand - if you download it all beforehand.";
		
		playerDesc [6] = "<size=38>Spotify</size>" +
			"\nListen to over 30 million songs instantly on your phone the world's most popular music streaming app. Choose " +
			"\nfrom all your favorite OPM & international hits and stream all you want." +
			"\n\nGet <size=34>FREE</size> Spotify with <size=34>Globe GoSURF</size>. No extra data charges when you listen. Text <size=34>GoSURF</size> to <size=34>8888</size>" +
			"\nor dial *143# to register.";
	}
	
	public Direction spinDirection() {
		return this.spinDir;
	}

	public void setRotateDone(bool isRotateDone) {
		this.iconsDoneRotating += 1;
		if(this.iconsDoneRotating == 8) {
			this.iconsDoneRotating = 0;
			this.rotateDone = isRotateDone;
			this.isSwiping 	= false;
		}
	}

	public bool getRotateDone() {
		return this.rotateDone;
	}
	
}
