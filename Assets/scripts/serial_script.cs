﻿using UnityEngine;
using System.Collections;
using System.Net.NetworkInformation;

public class serial_script : MonoBehaviour {
	
	private string txtSerial = "";
	public bool checkSerial = false;
	private serial_controller serialScript;
	public GameObject lblStatus;


	private Vector2 	firstPos 			= new Vector2 (0, 0);
	private Vector2 	lastPos 			= new Vector2 (0, 0);
	private float 		startTime;
	private float		endTime;
	private float 		finalPos;

	void Start () {

		serialScript = GameObject.Find("modal").GetComponent<serial_controller>();
		lblStatus = GameObject.Find("status");

		if(PlayerPrefs.HasKey("zoneSerial"))
		{
			string serial = PlayerPrefs.GetString("zoneSerial");
			serialScript.validateSerial(getMACAddress(),serial);
		}
		else
		{
			showModal();
			checkSerial = false;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetMouseButtonDown (0)) {
			firstPos.x 	= Input.mousePosition.x;
			firstPos.y 	= Input.mousePosition.y;
			startTime 	= Time.time; // Start timer on mouse down
		} else if (Input.GetMouseButtonUp (0)) {
			lastPos.x 	= Input.mousePosition.x;
			lastPos.y 	= Input.mousePosition.y;
			endTime 	= Time.time; // Save current timer 

			clickButton();
		}

	}

	void OnGUI()
	{
		if(checkSerial == true){
			hideModal();
			lblStatus.renderer.enabled = false;
		}
		else if(checkSerial == false)
			txtSerial = GUI.TextField (new Rect (Screen.width/2, (Screen.height/2) - Screen.height*.075f, Screen.width*.3f, Screen.height*.1f), txtSerial , 16);
			

	}

	void clickButton()
	{
		RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		
		if (hit.collider != null && hit.transform != null) {
			print (hit.collider.name);

			if(hit.collider.name == "ok_button"){

				if(txtSerial.Trim().Length == 16)
				{
					print (txtSerial);
					serialScript.validateSerial(getMACAddress(),txtSerial);
				}
				else
				{
					lblStatus.GetComponent<TextMesh>().text = "status: input serial";
				}
				//StartCoroutine(GetScores("B8-70-F4-37-AE-23", "4AA8FAB09581E5F1"));
			}

		}
	}



	public void showModal()
	{
		GameObject modal = GameObject.Find("modal");
		GameObject okButton = GameObject.Find("ok_button");
		GameObject lbl = GameObject.Find("lblSerial");

		modal.renderer.enabled = true;
		okButton.renderer.enabled = true;
		lbl.renderer.enabled = true;

	}
	public void hideModal()
	{
		GameObject modal = GameObject.Find("modal");
		GameObject okButton = GameObject.Find("ok_button");
		GameObject lbl = GameObject.Find("lblSerial");
        GameObject text = GameObject.Find("New Text");
		
		modal.renderer.enabled = false;
		okButton.renderer.enabled = false;
		lbl.renderer.enabled = false;
        text.renderer.enabled = false;
		
	}

	public string getMACAddress()
	{
		string macAdd = "";

		IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
		NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
		
		//foreach (NetworkInterface adapter in nics)
		//{
			PhysicalAddress address = nics[0].GetPhysicalAddress();
			byte[] bytes = address.GetAddressBytes();
			string mac = null;
			for (int i = 0; i < bytes.Length; i++)
			{
				mac = string.Concat(mac +(string.Format("{0}", bytes[i].ToString("X2"))));
				if (i != bytes.Length - 1)
				{
					mac = string.Concat(mac + "-");
				}
			}
			//macAdd += mac + "\n";
			macAdd += mac;
			
			
			//macAdd += "\n";
		//}

		return macAdd;
	}

}
