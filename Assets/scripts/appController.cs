﻿using UnityEngine;
using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class appController : MonoBehaviour {
    public static AppList AppShown { get; set; }
    public enum AppList { ZONE, CMS, SPLASH };
    public static bool CMS_Started { get; set; }
    public bool gameStarted { get; set; }

    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1234);

    public static appController Instance { get; private set; }

	// Use this for initialization
	IEnumerator Start () {
        Instance = this;
        CMS_Started = false;
        gameStarted = false;

        // Connect to the server
        while (!socket.Connected)
        {
            Send("connecting");
            ConnectToController();
            yield return new WaitForSeconds(5f);
        }
	}

    public void StartCMS()
    {
        Send("startCMS");
        Send("starting cms");
        AppShown = AppList.CMS;
        Send("showCMS"); // Added
        CMS_Started = true;
    }

    void ConnectToController()
    {
        try
        {
            socket.Connect(localEndPoint);
            Send("showCMS");
        }
        catch (Exception e)
        {
            LogError(e);
        }
    }

    public void Send(string data)
    {
        if (socket.Connected == true)
        {
            try
            {
                byte[] encodedData = Encoding.ASCII.GetBytes(data); // Encode string to bytes
                socket.Send(encodedData); // Send encoded data to server socket
            }
            catch (Exception e)
            {
                LogError(e);
            }
        }
    }

    public void DelayedSend(string data, float time)
    {
        StartCoroutine(DelayedSendRoutine(data, time));
    }

    IEnumerator DelayedSendRoutine(string data, float time)
    {
        yield return new WaitForSeconds(time);

        Send(data);
        appController.Instance.gameStarted = false;
        appController.AppShown = appController.AppList.CMS;
        yield return null;
    }

    void LogError(Exception error)
    {
        print(error.ToString());
    }
}
