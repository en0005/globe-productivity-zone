﻿using UnityEngine;
using System.Collections;

public class serial_controller : MonoBehaviour {

	private string secretKey = "mySecretKey"; // Edit this value and make sure it's the same as the one stored on the server
	public string highscoreURL = "http://114.198.129.245/sandbox/devteam/globekinect/validate.php?";
	public static string verifySerial;
	public bool doneCheck = false;
	private serial_script serialScript;
    GameObject kinectObject;

    void Awake()
    {
        kinectObject = GameObject.Find("KinectObject");
        kinectObject.SetActive(false);
    }

	void Start()
	{
		//StartCoroutine(PostScores(name));
		serialScript = GameObject.Find("modal").GetComponent<serial_script>();
	}
	
	// Get the scores from the MySQL DB to display in a GUIText.
	// remember to use StartCoroutine when calling this function!
	IEnumerator VerifySerial(string mac, string serial)
	{
		//string post_url = highscoreURL + "name=" + name;
	
		string macAdd = "mac="+mac;
		string key = "&serial="+serial;

		string post_url = highscoreURL + macAdd+key;
		//print("GET!");
		//gameObject.guiText.text = "Loading Scores";
		WWW hs_get = new WWW(post_url);
		yield return hs_get;
		
		if (hs_get.error != null)
		{
			print("There was an error getting the high score: " + hs_get.error);
		}
		else
		{
			//gameObject.guiText.text = hs_get.text;
			//print(hs_get.text);

			verifySerial = hs_get.text;
			print (verifySerial);

			if(verifySerial == "true"){
				serialScript.checkSerial = true;
				serialScript.lblStatus.GetComponent<TextMesh>().text = "status: accepted";
				PlayerPrefs.SetString("zoneSerial", serial);
				serialScript.hideModal();

                kinectObject.gameObject.SetActive(true);

				yield return new WaitForSeconds(2f);
				Destroy(gameObject);
			}
			else
            {
				serialScript.checkSerial = false;
				serialScript.lblStatus.GetComponent<TextMesh>().text = "status: invalid serial";
				serialScript.showModal();
			}

		}
	
	}
	
	public string Md5Sum(string strToEncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);
		
		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);
		
		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";
		
		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}
		
		return hashString.PadLeft(32, '0');
	}

	public void validateSerial(string mac, string serial)
	{
		StartCoroutine(VerifySerial(mac, serial));

	}

}
