﻿using UnityEngine;
using System.Collections;

public class cube_generator : MonoBehaviour {
	public Transform[] iconMesh = new Transform[5];
	public slingshot_script.iconType icons;
	public float lastSpawn;
	public Material[] iconMaterials = new Material[5];
    public Texture[] cloudTextures;

	private float cubeSize = 0.8f;
	private float resizeSpeed = 0.12f;
	private float timeToPlay = 60;
	private int previousType;

	private bool isLoadingCube 		= false;
	private bool isGameOver 		= false;
	private bool modalDisplayed 	= false;
	private bool[] allowedTypes 	= {true, true, true, true, true};
	private bool leftFolderDone 	= false;
	private bool middleFolderDone	= false;
	private bool rightFolderDone	= false;

	private slingshot_script[] scriptsPresent;
	private modal_script modalScript;
	private TextMesh timer;

	private Transform loadingPoint;
    private InteractionManager manager;

    GameObject[] goodPostIts = new GameObject[3];
    GameObject[] clouds = new GameObject[3];

	void Awake() {
		if(PlayerPrefs.HasKey("prodBestTime") == false)
			PlayerPrefs.SetInt("prodBestTime", 0);

		if(PlayerPrefs.HasKey("prodPlayAgain") == false)
			PlayerPrefs.SetInt ("prodPlayAgain", 0);

		loadingPoint = GameObject.Find ("loadedCubePoint").transform;
		scriptsPresent = FindObjectsOfType <slingshot_script>();
		modalScript = FindObjectOfType<modal_script> ();
		timer = GameObject.Find ("timer").GetComponent <TextMesh> ();
        Camera.main.transparencySortMode = TransparencySortMode.Orthographic;

		
		goodPostIts [0] = GameObject.Find ("good1");
		goodPostIts [1] = GameObject.Find ("good2");
		goodPostIts [2] = GameObject.Find ("good3");

		goodPostIts [0].transform.position = new Vector3 (-14, 6.08f, 2);
		goodPostIts [1].transform.position = new Vector3 (-14, 4.57f, 2);
		goodPostIts [2].transform.position = new Vector3 (-14, 3.12f, 2);

        clouds[0] = GameObject.Find("cloud_middle");
        clouds[1] = GameObject.Find("cloud_left");
        clouds[2] = GameObject.Find("cloud_right");
	}

	// Use this for initialization
	void Start () {
		generateCube ();
		//GameObject.Find ("exit").collider.enabled = true;
        Resources.UnloadUnusedAssets();
	}

	void OnApplicationQuit() {
		PlayerPrefs.SetInt ("prodPlayAgain", 0);
	}

	// User this to reset game / when playing again
	public void resetGame() {
		isGameOver = false;
		timeToPlay = 60; // reset timer
		timer.text = timeToPlay.ToString(); // reset timer text

		for(int i = 0; i < scriptsPresent.Length; i++) {
            try
            {
                Destroy(scriptsPresent[i].gameObject); // destroy all icons
            }
            catch (System.Exception e)
            {
                print(e.ToString());
            }
		}

		for(int i = 0; i < allowedTypes.Length; i++) {
            try
            {
                allowedTypes[i] = true; // set all allowed types(icons) to true
            }
            catch (System.Exception e)
            {
                print(e.ToString());
            }
		}

		FindObjectOfType<mechanics_script> ().showMechanics ();
	}

	void Update() {
        if (manager == null)
            manager = InteractionManager.Instance;

		if(timeToPlay > 0 && isGameOver == false) {
			timer.text = Mathf.RoundToInt((timeToPlay -= Time.deltaTime)).ToString();
            manager.SetDisableCursor(true);
		} else {
			if(modalDisplayed == false && isGameOver == false) {
				gameOver(false);
//				modalDisplayed = true;
			}
		}

        //KinectManager[] kinectObject = FindObjectsOfType<KinectManager> ();
        //if(kinectObject.Length > 1)
        //    Destroy(kinectObject[1].gameObject);

		slingshot_script iconScript = FindObjectOfType<slingshot_script> ();
		scriptsPresent = FindObjectsOfType<slingshot_script>();

        if (countEnabledTypes(allowedTypes) > 0 && iconScript != null)
        {
			if(iconScript.transform.localScale.x != cubeSize) {
				Vector3 newScale = iconScript.transform.localScale;
				float newVal = Mathf.Lerp(newScale.x, cubeSize, resizeSpeed + Time.deltaTime);
				newScale = new Vector3(newVal, newVal, newVal);
				
				iconScript.transform.localScale = Vector3.Lerp(iconScript.transform.localScale, newScale, 1f);
				if(iconScript.transform.localScale.x > (cubeSize - (cubeSize * 0.04f))) {
					iconScript.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);
				}
			} else {
				if(scriptsPresent.Length <= 1) {
					if(isLoadingCube == false) 
						StartCoroutine("loadCube");
				}

			}
		} else {
			if(modalDisplayed == false) {
				gameOver (true);
				modalDisplayed = true;
			}
		}

		if(allowedTypes[0] == false && middleFolderDone == false) {
			StartCoroutine (stickPostIt(0));
			middleFolderDone = true;
		}
		if(allowedTypes[1] == false && allowedTypes[2] == false && leftFolderDone == false) {
			StartCoroutine (stickPostIt(1));
			leftFolderDone = true;
		}
		if(allowedTypes[3] == false && allowedTypes[4] == false && rightFolderDone == false) {
			StartCoroutine (stickPostIt(2));
			rightFolderDone = true;
		}
	}

	void gameOver(bool playerWon) {
        if (playerWon)
            PlayerPrefs.SetInt("playerWon", 1);
        else
            PlayerPrefs.SetInt("playerWon", 0);

        tagline_script.timeToPlay = timeToPlay;

        //BoxCollider[] childrenColliders = GetComponentsInChildren<BoxCollider> ();
        //foreach(BoxCollider buttonCollider in childrenColliders) {
        //    buttonCollider.enabled = true;
        //}

        //slingshot_script[] iconsArray = FindObjectsOfType<slingshot_script>();
        //for (int i = 0; i < iconsArray.Length; i++)
        //{
        //    try
        //    {
        //        Destroy(iconsArray[i].gameObject);
        //    }
        //    catch (System.Exception e)
        //    {
        //        print(e.ToString());
        //    }
        //}

        //if (playerWon == false)
        //{
        //    GameObject.Find("text_play").GetComponent<TextMesh>().text = "try";
        //    GameObject.Find("player_score").GetComponent<MeshRenderer>().enabled = false;
        //    GameObject.Find("your_time").GetComponent<MeshRenderer>().enabled = false;

        //    GameObject.Find("failed_text").GetComponent<MeshRenderer>().enabled = true;
        //    GameObject.Find("failed_text2").GetComponent<MeshRenderer>().enabled = true;

        //    //			GameObject.Find ("game_over_text").GetComponent<TextMesh>().text = "Game Over";
        //    //			Application.LoadLevel (Application.loadedLevelName);
        //}
        //else if (playerWon == true)
        //{
        //    GameObject.Find("game_over_text").GetComponent<TextMesh>().text = "";
        //    int newTime = (int)(Mathf.Floor(60 - timeToPlay));
        //    if (PlayerPrefs.GetInt("prodBestTime") > newTime)
        //    {
        //        PlayerPrefs.SetInt("prodBestTime", newTime);
        //        GameObject.Find("new_best_icon").renderer.enabled = true;
        //    }
        //    else
        //    {
        //        GameObject.Find("new_best_icon").renderer.enabled = false;
        //    }

        //    GameObject.Find("player_score").GetComponent<TextMesh>().text = newTime.ToString();
        //}

        //isGameOver = true;
        //if (!Camera.main.orthographic)
        //{
        //    Camera.main.transparencySortMode = TransparencySortMode.Orthographic;
        //    Camera.main.orthographic = true; // Set camera to orthographic
        //}

        //if (modalDisplayed == false)
        //{
        //    modalScript.showModal();

        //    GameObject.Find("modal_dim_tag_line").GetComponent<tagline_script>().enabled = true;
        //    modalDisplayed = true;
        //}

        Application.LoadLevel(Level.GAME_OVER);
	}

	IEnumerator stickPostIt(int postItIndex) {
		GameObject activePostIt = null;
        GameObject activeCloud = null;
 		if(postItIndex == 0) {
			activePostIt = goodPostIts[0];
            activeCloud = clouds[0];
		} else if (postItIndex == 1) {
			activePostIt = goodPostIts[1];
            activeCloud = clouds[1];
		} else if(postItIndex == 2) {
			activePostIt = goodPostIts[2];
            activeCloud = clouds[2];
		}
       // activeCloud.renderer.enabled = true;

		Vector3 postPosition = activePostIt.transform.position;
		postPosition.x = -10.2f;
		while(activePostIt.transform.position != postPosition) {
			activePostIt.transform.position = Vector3.Lerp (activePostIt.transform.position, postPosition, 0.1f + Time.deltaTime);
		}

        int currentCloudTexture = 0;
        float cloudTimer = 0f;
        while (currentCloudTexture != 7)
        {
            cloudTimer += Time.fixedDeltaTime;
            print(cloudTimer + " | " + cloudTimer % 1.02f);
            if (cloudTimer >= 1f)
            {
                cloudTimer = 0;
                currentCloudTexture += 1;
                activeCloud.renderer.material.mainTexture = cloudTextures[currentCloudTexture];
            }

            yield return null;
        }

		yield return null;
	}

	IEnumerator loadCube() {
		isLoadingCube = true;

		while(scriptsPresent[0].transform.position != loadingPoint.position) {
			scriptsPresent[0].transform.position = Vector3.Lerp (scriptsPresent[0].transform.position, loadingPoint.position, 0.1f + Time.deltaTime);
			scriptsPresent[0].transform.rotation = Quaternion.Lerp (scriptsPresent[0].transform.rotation, loadingPoint.rotation, 0.1f + Time.deltaTime);

			if((scriptsPresent[0].transform.position - loadingPoint.position).sqrMagnitude < 0.002f)
				scriptsPresent[0].transform.position = loadingPoint.position;
				
			yield return null;
		}
		scriptsPresent [0].activateCube ();

		generateCube ();
		isLoadingCube = false;
		yield return null;
	}
	
	public void generateCube() {
		if(scriptsPresent.Length < 2) {
			int rand = 0;
			if(countEnabledTypes(allowedTypes) == 1) {
				rand = previousType;
			} else if (countEnabledTypes(allowedTypes) >= 2) {
				while(rand == previousType || allowedTypes[rand] == false)
					rand = Random.Range (0, 5);
			}

			previousType = rand; // Set the previous type to avoid repetition
			/* 0 gmail
			 * 1 hangouts
			 * 2 drive
			 * 3 sites
			 * 4 calendar
			 */
			slingshot_script iconScript = cubeInstantiate(rand);
			switch(rand) {
				case 1:
					iconScript.setIconType(slingshot_script.iconType.hangouts);
					break;
					
				case 2:
					iconScript.setIconType(slingshot_script.iconType.drive);
					break;
					
				case 3:
					iconScript.setIconType(slingshot_script.iconType.sites);
					break;
					
				case 4:
					iconScript.setIconType(slingshot_script.iconType.calendar);
					break;
					
				default:
					iconScript.setIconType(slingshot_script.iconType.gmail);
					break;
			}
		}
	}

	slingshot_script cubeInstantiate(int rand) {
		Instantiate (iconMesh[rand], transform.position, transform.rotation);

		lastSpawn = Time.time;
		
		slingshot_script iconScript = FindObjectOfType<slingshot_script> ();
		iconScript.transform.localScale = new Vector3(0, 0, 0);

		return iconScript;
	}

	public void removeType(slingshot_script.iconType iconType) {
		switch(iconType) {
			case slingshot_script.iconType.hangouts:
				this.allowedTypes [1] = false;
				break;
				
			case slingshot_script.iconType.drive:
				this.allowedTypes [2] = false;
				break;
				
			case slingshot_script.iconType.sites:
				this.allowedTypes [3] = false;
				break;
				
			case slingshot_script.iconType.calendar:
				this.allowedTypes [4] = false;
				break;
				
			default:
				this.allowedTypes [0] = false;
				break;
		}

		print (countEnabledTypes(allowedTypes));
	}

	int countEnabledTypes(bool[] haystack) {
		int enabledTypes = 0;
		for(int i = 0; i < haystack.Length; i++) {
			if(haystack[i] == true)
				enabledTypes++;
		}
		
		return enabledTypes;
	}

	bool indexOf(int needle, int[] haystack) {
		foreach(int value in haystack) {
			if(haystack[value] == needle)
				return true;
		}

		return false;
	}

	public void setModalDiplayed(bool isModalDisplayed) {
		modalDisplayed = isModalDisplayed;
	}

	public float getTimePlayed() {
		return timeToPlay;
	}

}
