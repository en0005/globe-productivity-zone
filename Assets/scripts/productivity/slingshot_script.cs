using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class slingshot_script : MonoBehaviour {
    // Added by EAV
    public float dragSpeed = 25.5f;
    public GameObject
        handCursor,
        DebugGUI;

    private InteractionManager manager;
    private KinectManager Kmanager;

    private GameObject 
        draggedObject,
        splashObject;

    Vector3
        draggedObjectOffset,
        screenNormalPos     = Vector3.zero,
        screenPixelPos      = Vector3.zero,
        screenHandPos       = Vector3.zero;

    private Queue<double> 
        _weightedX = new Queue<double>(),
        _weightedY = new Queue<double>();

    Ray KinectRay;
    RaycastHit Rayhit;

    bool 
        isDragging      = false,
        isGrip          = false,
        UseKinect       = true,
        gripOnce,
        isReleased,
        isLeftHandDrag,
        isHitAvctiveObj = false;

    Texture texture = null;

    float 
        Distance,
        PrevDistance = 0f,
        YDistance,
        PrevYDistance,
        offsetStrength = 0f,
        force = 0f;

    Animator anim;
    // Added by EAV

	public float maxStretch = 3f;
	public AudioClip[] sfx;

	GameObject 	
        creator,
        slingshot,
        currentPower,
        currentPowerArrow;

	iconType currentIconType;

	Vector3 
        oldCubePosition,
        mouseStartPosition,
        lastAllowedPosition;

	float 
        stretchStrength,
        maxStretchSqr;

	bool 
        isActive    = false,
        isDraggable = false,
        dragging    = false;

	private Ray ray;
	private RaycastHit hit;

	private enum targetFolder {left, middle, right};
	public enum iconType {gmail, hangouts, drive, sites, calendar};
	private targetFolder currentTarget = targetFolder.middle;

	void Awake() {
		creator = GameObject.Find ("cubeSpawn");
		currentPower = GameObject.Find ("current_power");
		currentPowerArrow = GameObject.Find ("power_arrow");
		this.collider.enabled = false;
	}

	void Start () {
		maxStretchSqr = maxStretch * maxStretch;
		oldCubePosition = transform.position;
		slingshot = GameObject.Find ("newSling");

        // EAV
        handCursor = GameObject.Find("handcursor");
        anim = handCursor.GetComponent<Animator>();
        DebugGUI = GameObject.Find("DebugGUI");
        // EAV
	}

	void Update() {
        //KinectController();
        //MouseController();
	}
    void MouseController()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (this.isActive == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (collider.Raycast(ray, out hit, 100.00f))
                {
                    this.dragging = true;
                    audio.clip = sfx[0];
                    audio.pitch = UnityEngine.Random.Range(.8f, 1f);
                    audio.Play();
                    mouseStartPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }
                else
                {
                    this.dragging = false;
                }

            }
            else if (Input.GetMouseButton(0))
            {
                if (this.dragging == true && isDraggable == true)
                {
                    Vector3 currentMousePosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    Vector3 slingshotToMouse = GameObject.Find("newSling").transform.position + Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    Vector3 calculatedPosition = currentMousePosition - mouseStartPosition;

                    if (calculatedPosition.x > .08f)
                    {
                        calculatedPosition.x = .15f;
                        currentTarget = targetFolder.left;
                    }
                    else if (calculatedPosition.x < -0.08f)
                    {
                        calculatedPosition.x = -.15f;
                        currentTarget = targetFolder.right;
                    }
                    else
                    {
                        calculatedPosition.x = 0;
                        currentTarget = targetFolder.middle;
                    }

                    calculatedPosition *= 16;

                    stretchStrength = -(calculatedPosition.y / 2.25f);
                    stretchStrength *= 100;
                    stretchStrength = Mathf.RoundToInt(stretchStrength);

                    if (stretchStrength <= 100 && stretchStrength >= 0)
                    {
                        lastAllowedPosition = transform.position;
                    }
                    else if (stretchStrength < 0)
                    {
                        stretchStrength = 0;
                        calculatedPosition.y = 0;
                    }
                    else if (stretchStrength >= 100)
                    {
                        stretchStrength = 100;
                        calculatedPosition.y = lastAllowedPosition.y;
                    }

                    force = 22f * (stretchStrength / 100);

                    float strechStrengthPercentage = stretchStrength / 100;
                    // 8f is the max Y scale of the power bar
                    float tempValue = 8f * strechStrengthPercentage; 						// Calculate the scale for the power bar
                    currentPower.transform.localScale = new Vector3(0.8f, tempValue, 1f); 					// Set the new scale for the power bar dependig on the stretch
                    currentPower.transform.position = new Vector3(-22.5f, -4f + (tempValue / 2), 14); 	// Set the new position for the power bar
                    currentPowerArrow.transform.position = new Vector3(-20.75f, -3.65f + (tempValue * 0.925f), 12); 	// Set the new position for the power bar

                    calculatedPosition.z = calculatedPosition.y;
                    if (currentTarget == targetFolder.middle)
                        calculatedPosition.z = calculatedPosition.y * 1.5f;

                    calculatedPosition.y = 0;
                    transform.position = Vector3.Lerp(transform.position, oldCubePosition + calculatedPosition, 15 * Time.deltaTime);
                }
            }
            else if (Input.GetMouseButtonUp(0) && this.dragging == true)
            {
                if (isDraggable == true)
                {
                    rigidbody.useGravity = true;
                    audio.clip = sfx[6];
                    audio.Play();
                    oldCubePosition = transform.position;

                    //transform.rigidbody.freezeRotation = false;
                    StartCoroutine("initiateFlight");
                    this.dragging = false;
                }

                this.isDraggable = false;
            }

            if (rigidbody.useGravity == true)
            {
                switch (currentTarget)
                {
                    case targetFolder.left:
                        rigidbody.AddForce((GameObject.Find("folder_left").transform.position - transform.position).normalized * force);
                        break;

                    case targetFolder.right:
                        rigidbody.AddForce((GameObject.Find("folder_right").transform.position - transform.position).normalized * force);
                        break;

                    default:
                        rigidbody.AddForce((GameObject.Find("folder_middle").transform.position - transform.position).normalized * force);
                        break;
                }
            }
        }
    }
    // EAV
    void KinectController()
    {
        if (manager != null && manager.IsInteractionInited() && this.isActive && manager.isDisableCursor())
        {

            if (Kmanager == null)
                Kmanager = KinectManager.Instance;

            if (Kmanager.IsUserDetected())
            {

                screenHandPos = manager.GetLastCursorPos();
                // ----------------------
                if (manager.IsLeftHandPrimary())
                {

                    screenNormalPos = manager.GetLeftHandScreenPos();
                    // if the left hand is primary, check for left hand grip
                    if (manager.GetLastLeftHandEvent() == InteractionWrapper.InteractionHandEventType.Grip)
                    {
                        anim.Play("LeftHandGrip");
                        isLeftHandDrag = true;
                        isGrip = true;
                        if (!gripOnce)
                        {
                            Distance = JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft));
                            YDistance = Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft).y;
                            gripOnce = true;
                        }
                        else
                        {
                            PrevDistance = JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft));
                            PrevYDistance = Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft).y;
                        }
                    }
                    else
                    {
                        anim.Play("LeftHandIdle");
                        isGrip = false;
                        gripOnce = false;
                    }

                }
                else if (manager.IsRightHandPrimary())
                {
                    screenNormalPos = manager.GetRightHandScreenPos();
                    // if the right hand is primary, check for right hand grip
                    if (manager.GetLastRightHandEvent() == InteractionWrapper.InteractionHandEventType.Grip)
                    {
                        anim.Play("RightHandGrip");
                        //DebugGUI.guiText.text = "GRIP"; 
                        isLeftHandDrag = false;
                        isGrip = true;
                        if (!gripOnce)
                        {
                            Distance = JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight));
                            YDistance = Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight).y;
                            gripOnce = true;
                        }
                        else
                        {
                            PrevYDistance = Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight).y;
                            PrevDistance = JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight));
                        }
                    }
                    else
                    {
                        anim.Play("RightHandIdle");
                        isGrip = false;
                        gripOnce = false;
                    }
                }
                else
                {
                    screenNormalPos = manager.GetLastCursorPos();
                }

                screenNormalPos.z = -2.5f;

                isReleased = isLeftHandDrag ? (manager.GetLastLeftHandEvent() == InteractionWrapper.InteractionHandEventType.Release) :
                   (manager.GetLastRightHandEvent() == InteractionWrapper.InteractionHandEventType.Release);
                // convert the normalized screen pos to pixel pos
                screenPixelPos.x = (int)(screenNormalPos.x * Camera.main.pixelWidth);
                screenPixelPos.y = (int)(screenNormalPos.y * Camera.main.pixelHeight);

                Ray ray = Camera.main.ScreenPointToRay(screenPixelPos);
                Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, 0));
                float distance;
                xy.Raycast(ray, out distance);
                Vector3 newCursorPos = ray.GetPoint(distance);

                DebugGUI.guiText.text = "CURSOR : " + newCursorPos.ToString();
                newCursorPos = ExponentialWeightedAvg(newCursorPos);
                newCursorPos = Vector3.Lerp(handCursor.transform.position, newCursorPos, dragSpeed * Time.deltaTime);
                newCursorPos.z = -2.5f;
                if ((Math.Abs(newCursorPos.x) < 7) && (Math.Abs(newCursorPos.y) < 4.5))
                    manager.SetLastCursorPos(newCursorPos);
                else
                    newCursorPos = manager.GetLastCursorPos();

                handCursor.transform.position = newCursorPos;
            }
        }
        else
        {
            isReleased = false;
            isGrip = false;
        }

        if (isActive == true)
        {
            //screenPixelPos.x -= 20;
            //screenPixelPos.y -= 10;
            ray = Camera.main.ScreenPointToRay(screenPixelPos);

            if (isGrip & !this.dragging)
            {

                if (collider.Raycast(ray, out hit, 100.00f))
                {

                    this.dragging = true;
                    audio.clip = sfx[0];
                    audio.pitch = UnityEngine.Random.Range(.8f, 1f);
                    audio.Play();
                    mouseStartPosition = Camera.main.ScreenToViewportPoint(screenPixelPos);

                }
                else
                {
                    this.dragging = false;
                }
            }
            else if ((this.dragging) && (!isReleased))
            {

                if (this.dragging == true && isDraggable == true)
                {
                    Vector3 currentMousePosition = Camera.main.ScreenToViewportPoint(screenPixelPos);
                    // Vector3 slingshotToMouse = GameObject.Find("newSling").transform.position + Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    Vector3 calculatedPosition = currentMousePosition - mouseStartPosition;

                    if (calculatedPosition.x > 0.35f)
                    {
                        currentTarget = targetFolder.right;
                        offsetStrength = 20f;
                    }
                    else if (calculatedPosition.x < -0.35f)
                    {
                        currentTarget = targetFolder.left;
                        offsetStrength = 20f;
                    }
                    else
                    {
                        calculatedPosition.x = 0;
                        currentTarget = targetFolder.middle;
                        offsetStrength = 15f;
                    }

                    calculatedPosition *= 16;

                    // Compute power base on the distance between shoulder and the hand
                    float total = ((Distance - PrevDistance) * offsetStrength) * 100;
                    if (total < 20)
                    {
                        total = ((YDistance - PrevYDistance) * offsetStrength) * 100;
                    }

                    if (total > 100)
                        total = 100;
                    if (total < 0)
                        total = 0;
                    stretchStrength = Mathf.RoundToInt(total);

                    float calculatedX = 3 * (total / 100);
                    if (calculatedPosition.x > 0f)
                    {
                        if (total == 100)
                            calculatedPosition.x = 4.5f;
                        else
                            calculatedPosition.x =  7 - calculatedX;
                    }
                    if (calculatedPosition.x < 0f)
                    {
                        if (total == 100)
                            calculatedPosition.x = -4.5f;
                        else
                            calculatedPosition.x =  -(7 - calculatedX);
                    }

                    //DebugGUI.guiText.text = "calculatedPosition.x: " + calculatedPosition.x.ToString();

                    ////if (total == 100)
                    //float calculatedX = 3f * (total / 100);

                    if ((calculatedPosition.x > 4f) && (total == 100))
                        calculatedPosition.x = 4f;
                    
                    if ((calculatedPosition.x < -4f) && (total == 100))
                        calculatedPosition.x = -4f;

                    //DebugGUI.guiText.text = "Strength : " + stretchStrength.ToString();
                    // 8f is the max Y scale of the power bar
                    //float tempValue = 8f * (stretchStrength / 100); 					// Calculate the scale for the power bar
                    //currentPower.transform.localScale = new Vector3(1f, tempValue, 1f); 					// Set the new scale for the power bar dependig on the stretch
                    //currentPower.transform.position = new Vector3(-21.5f, -4f + (tempValue / 2), 14); 	// Set the new position for the power bar
                    //currentPower.renderer.material.mainTextureScale = new Vector2(1f, stretchStrength / 100); 			// Calculate the new material scale
                    //currentPower.renderer.material.mainTextureOffset = new Vector2(1f, 105 - (stretchStrength / 100)); 	// Set the new material offset

                    float strechStrengthPercentage = stretchStrength / 100;
                    // 8f is the max Y scale of the power bar
                    float tempValue = 8f * strechStrengthPercentage; 						// Calculate the scale for the power bar
                    currentPower.transform.localScale = new Vector3(0.8f, tempValue, 1f); 					// Set the new scale for the power bar dependig on the stretch
                    currentPower.transform.position = new Vector3(-22.5f, -4f + (tempValue / 2), 14); 	// Set the new position for the power bar
                    currentPowerArrow.transform.position = new Vector3(-20.75f, -3.65f + (tempValue * 0.925f), 12); 	// Set the new position for the power bar

                    force = 22f * (stretchStrength / 100);

                    float zpos = (5 * total) / 100;
                    calculatedPosition.z = -zpos;

                    calculatedPosition.y = 0;
                    if (currentTarget == targetFolder.left)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        transform.rotation = Quaternion.Euler(0, -205f, 0);
                    }
                    else if (currentTarget == targetFolder.right)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        transform.rotation = Quaternion.Euler(0, 205f, 0);
                    }
                    else
                        transform.rotation = Quaternion.Euler(0, -180f, 0);

                    transform.position = Vector3.Lerp(transform.position, oldCubePosition + calculatedPosition, 15 * Time.deltaTime);
                    //Vector3 ActualObjPos = transform.position;
                    //if ((Math.Abs(ActualObjPos.x) > 3.2f) && (Math.Abs(ActualObjPos.z) >= 4.4f))
                    //{
                    //    if (ActualObjPos.x < 0)
                    //        ActualObjPos.x = -3.2f;
                    //    else
                    //        ActualObjPos.x = 3.2f;
                    //    DebugGUI.guiText.text = "POS : " + transform.position.ToString();
                    //}
                    //transform.position = ActualObjPos;

                    //Vector3 pos = Camera.main.WorldToViewportPoint(transform.renderer.bounds.center);
                    // Position hand to the object
                    Vector3 handpos = transform.position;
                    handpos.z -= 0.5f;

                    handCursor.transform.position = handpos;
                }
            }
            else if (isReleased && this.dragging)
            {
                if (isDraggable == true)
                {
                    rigidbody.useGravity = true;
                    audio.clip = sfx[6];
                    audio.Play();
                    oldCubePosition = transform.position;

                    transform.rigidbody.freezeRotation = false;
                    StartCoroutine("initiateFlight");
                    this.dragging = false;
                }

                this.isDraggable = false;
            }

            if (rigidbody.useGravity == true)
            {
                switch (currentTarget)
                {
                    case targetFolder.left:
                        rigidbody.AddForce((GameObject.Find("folder_left").transform.position - transform.position).normalized * force);
                        break;

                    case targetFolder.right:
                        rigidbody.AddForce((GameObject.Find("folder_right").transform.position - transform.position).normalized * force);
                        break;

                    default:
                        rigidbody.AddForce((GameObject.Find("folder_middle").transform.position - transform.position).normalized * force);
                        break;
                }
            }
        }
    }
    // ---- END Kinect Controller (EAV)
    
	void FixedUpdate() {
        KinectController();
		if(transform.position.z > 20f || transform.position.y < -5f || transform.position.y > 10f  || transform.position.x > 16f || transform.position.z < -16f ) {
			Destroy(gameObject);
			GameObject.Find ("cubeSpawn").GetComponent<cube_generator>().generateCube();
		}
	}

	void OnCollisionEnter(Collision collision) {
		collider.enabled = false;
		rigidbody.drag = 0.5f;
		audio.clip = sfx[UnityEngine.Random.Range(1, 5)];
		audio.pitch = 1f;
		audio.Play();
		force = 0f;

		StartCoroutine (destroyCube(collision));
	}

	IEnumerator initiateFlight() {
		while(transform.position.y < 2) {
			rigidbody.AddForce(Vector3.up * (force * (stretchStrength / 150)));
			yield return null;
		}
	}

	IEnumerator destroyCube(Collision collision) {
		//particleSystem.Play ();
		if (collision.transform.name == "folder_left") {
			if(this.currentIconType == iconType.drive || this.currentIconType == iconType.hangouts) {
				StartCoroutine (correctFolderAnimation(collision.gameObject, this.currentIconType));
			}
		} else if (collision.transform.name == "folder_middle") {
			if(this.currentIconType == iconType.gmail) {
				StartCoroutine (correctFolderAnimation(collision.gameObject, this.currentIconType));
			}
		} else if (collision.transform.name == "folder_right") {
			if(this.currentIconType == iconType.sites || this.currentIconType == iconType.calendar) {
				StartCoroutine (correctFolderAnimation(collision.gameObject, this.currentIconType));
			}
		} else {
			yield return new WaitForSeconds(.8f);
			Destroy (gameObject);
		}

		yield return null;
	}

	IEnumerator correctFolderAnimation(GameObject folder, iconType icon) {
		rigidbody.useGravity = false;
		Vector3 topOfFolder = folder.transform.position;
		topOfFolder.y += 4f;

		Quaternion resetRotation = new Quaternion(0f, 180f, 0f, 0f);

		// Move icon on top of folder
		//while(transform.position != topOfFolder) {
			//float speed = 2f;
			//if((transform.position - topOfFolder).magnitude < .3f)
				//speed = 3f;
			//if((transform.position - topOfFolder).magnitude > 1)
				//speed *= (transform.position - topOfFolder).magnitude;

			// If overall distance is less than 0.4 units
			//if((transform.position - topOfFolder).magnitude < 0.5f)
				transform.position = topOfFolder;

			//transform.position = Vector3.MoveTowards (transform.position, topOfFolder, speed + Time.deltaTime);
            //transform.rotation = Quaternion.Lerp(transform.rotation, resetRotation, 0.075f + Time.deltaTime);
                transform.rotation = resetRotation;

			//yield return null;
		//}

		// Insert icon into folder and destroy it
		while(transform.position != folder.transform.position) {
			transform.position = Vector3.Lerp (transform.position, folder.transform.position, 0.1f + Time.deltaTime);
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0f, 0f, 0f), 0.06f + Time.deltaTime);

			if(transform.localScale.x < 0.2f) {
				creator.GetComponent<cube_generator> ().removeType (icon);
				Destroy (gameObject);
			}

			yield return null;
		}
		
		yield return null;
	}

	public void setIconType(iconType type) {
		this.currentIconType = type;
	}

	public iconType getIconType() {
		return this.currentIconType;
	}

	public void activateCube() {
		oldCubePosition = transform.position;
		this.isDraggable = true;
		this.isActive = true;
		this.collider.enabled = true;

        // EAV
        this.manager = InteractionManager.Instance;
        this.Kmanager = KinectManager.Instance;
	}

    // Added by EAV
    // Additional Functions

    private double ExponentialMovingAverage(double[] data, double baseValue)
    {
        double numerator = 0;
        double denominator = 0;

        double average = 0;
        for (int i = 0; i < data.Length; i++)
            average += data[i];

        average /= data.Length;

        for (int i = 0; i < data.Length; ++i)
        {
            numerator += data[i] * Math.Pow(baseValue, data.Length - i - 1);
            denominator += Math.Pow(baseValue, data.Length - i - 1);
        }

        numerator += average * Math.Pow(baseValue, data.Length);
        denominator += Math.Pow(baseValue, data.Length);

        return numerator / denominator;
    }

    private Vector3 ExponentialWeightedAvg(Vector3 joint)
    {
        _weightedX.Enqueue(joint.x);
        _weightedY.Enqueue(joint.y);

        if (_weightedX.Count > 5)
        {
            _weightedX.Dequeue();
            _weightedY.Dequeue();
        }

        double x = ExponentialMovingAverage(_weightedX.ToArray(), 0.9);
        double y = ExponentialMovingAverage(_weightedY.ToArray(), 0.9);

        Vector3 _return = Vector2.zero;
        _return.x = (float)x;
        _return.y = (float)y;
        _return.z = -2.5f;

        return _return;
    }

    private float JointDistance(Vector3 joint1, Vector3 joint2)
    {
        float dX = joint1.x - joint2.x;
        float dY = joint1.y - joint2.y;
        float dZ = joint1.z - joint2.z;
        return (float)Math.Sqrt((dX * dX) + (dY * dY) + (dZ * dZ));
    }
}