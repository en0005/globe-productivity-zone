﻿using UnityEngine;
using System.Collections;

public class tagline_script : MonoBehaviour {

	Vector3 
        initialPostion,
        finalPosition,
        maxSize = new Vector3 (1, 1, 1),
        minSize = new Vector3(0, 0, 1);

	prod_buttons nextButton;

    public static float timeToPlay;

	// Use this for initialization
	void Start () {
		GameObject.Find("handcursor").GetComponent<CursorInteraction>().audio.Stop();
		audio.clip = GameObject.Find("handcursor").GetComponent<CursorInteraction>().audioClip[2];
		audio.Play();

        nextButton = gameObject.GetComponentInChildren<prod_buttons>(); // Get the child next button

		initialPostion = transform.position;
		finalPosition = initialPostion;
		finalPosition.x = -21;

		StartCoroutine ("showModal");

        bool playerWon;
        if (PlayerPrefs.GetInt("playerWon") == 1)
            playerWon = true;
        else
            playerWon = false;


        if (playerWon == false)
        {
            GameObject.Find("text_play").GetComponent<TextMesh>().text = "try";
            GameObject.Find("player_score").GetComponent<MeshRenderer>().enabled = false;
            GameObject.Find("your_time").GetComponent<MeshRenderer>().enabled = false;

            GameObject.Find("failed_text").GetComponent<MeshRenderer>().enabled = true;
            GameObject.Find("failed_text2").GetComponent<MeshRenderer>().enabled = true;

            //			GameObject.Find ("game_over_text").GetComponent<TextMesh>().text = "Game Over";
            //			Application.LoadLevel (Application.loadedLevelName);
        }
        else if (playerWon == true)
        {
            //GameObject.Find("game_over_text").GetComponent<TextMesh>().text = "";
            int newTime = (int)(Mathf.Floor(60 - timeToPlay));
            if (PlayerPrefs.GetInt("prodBestTime") > newTime)
            {
                PlayerPrefs.SetInt("prodBestTime", newTime);
                GameObject.Find("new_best_icon").renderer.enabled = true;
            }
            else
            {
                GameObject.Find("new_best_icon").renderer.enabled = false;
            }

            GameObject.Find("player_score").GetComponent<TextMesh>().text = newTime.ToString();
        }
	}

	public void moveLeft() {
		StartCoroutine ("moveLeftRoutine");
	}

	IEnumerator showModal() {
		while(transform.localScale != maxSize) { // Scale up the modal into view
			transform.localScale = Vector3.Lerp (transform.localScale, maxSize, 0.1f + Time.deltaTime);

			yield return null;
		}

        nextButton.collider.enabled = true;
        nextButton.transform.parent = null;

		yield return null;
	}

	IEnumerator moveLeftRoutine() {
        nextButton.transform.parent = nextButton.parentObject.transform;
		nextButton.collider.enabled = false; // Disable the next button to avoid being clicked again
		GameObject gameOverModal = GameObject.Find ("modal_dim_game_over");
		gameOverModal.transform.localScale = maxSize;

		prod_buttons[] gameOverButtons = gameOverModal.GetComponentsInChildren<prod_buttons> ();
		for(int i = 0; i < gameOverButtons.Length; i++) {
			gameOverButtons[i].collider.enabled = true;
		}

		while(transform.position != finalPosition) { // Move both modals to the left
			transform.position = Vector3.Lerp (transform.position, finalPosition, 0.1f + Time.deltaTime);
			gameOverModal.transform.position = Vector3.Lerp (gameOverModal.transform.position, initialPostion, 0.1f + Time.deltaTime);
			
			yield return null;
		}

        foreach (prod_buttons btn in gameOverModal.transform.GetComponentsInChildren<prod_buttons>())
        {
            btn.parentObject = btn.transform.parent.gameObject;
            btn.collider.enabled = true;
            btn.transform.parent = null;
        }

		//Destroy (gameObject);
		yield return null;
	}
}
