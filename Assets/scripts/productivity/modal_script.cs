﻿using UnityEngine;
using System.Collections;

public class modal_script : MonoBehaviour {
	Vector3 minSize = new Vector3 (0, 0, 0);
	Vector3 maxSize = new Vector3 (1, 1, 1);

	void Update() {
		//if(transform.localScale.x == 0)
			//FindObjectOfType<cube_generator> ().setModalDiplayed(false);
	}

	public void showModal() {
        Application.LoadLevel(Level.GAME_OVER);
        //if (transform.localScale.x == 0)
        //{
        //    StartCoroutine("showModalRoutine");
        //}
	}

	IEnumerator showModalRoutine() {
		while(transform.localScale.x != 1) {
			transform.localScale = Vector3.Lerp (transform.localScale, maxSize, 0.1f + Time.deltaTime);
			if((transform.localScale - maxSize).magnitude < 0.06f)
				transform.localScale = maxSize;

			yield return null;
		}

		yield return null;
	}
}