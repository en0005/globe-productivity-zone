﻿using UnityEngine;
using System.Collections;

public class boss_script : MonoBehaviour {
	public Texture[] callingTextures = new Texture[2];

	Vector3 maxSize;
	Vector3 minSize = new Vector3(3, 0, 1);
	Vector3 newPosition = new Vector3(7.64037f, -1.194521f, 2);
	Vector3 oldPosition = new Vector3(7.64037f, -2.94452f, 2);
//	Vector3 oldPosition = new Vector3(7.64037f, -4.63049f, 2);

	bool firstCallInitiated = false;
	bool secondCallInitiated = false;

	float animationSpeed = 8f;
	float movementSpeed = 4f;

	cube_generator cubeGenerator;

	// Use this for initialization
	void Start () {
		maxSize = transform.localScale;
		transform.localScale = minSize;

		cubeGenerator = FindObjectOfType<cube_generator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(cubeGenerator.getTimePlayed() < Random.Range(15, 21) && secondCallInitiated == false) {
			// Show the second call
			StartCoroutine(bossCallingRoutine(false));
			secondCallInitiated = true;
		} else if(cubeGenerator.getTimePlayed() < Random.Range(32, 39) && firstCallInitiated == false) {
			// Show the first call
			StartCoroutine(bossCallingRoutine(false));
			firstCallInitiated = true;
		}
	}

	IEnumerator bossCallingRoutine(bool firstCall) {
		if(firstCall == true) {
			renderer.material.SetTexture("_MainTex", callingTextures[0]);
		} else if(firstCall == false) {
			renderer.material.SetTexture("_MainTex", callingTextures[1]);
		}

		while(transform.localScale != maxSize) { // Show the boss calling
			transform.localScale = Vector3.MoveTowards(transform.localScale, maxSize, animationSpeed * Time.deltaTime);
			transform.position = Vector3.MoveTowards (transform.position, newPosition, movementSpeed * Time.deltaTime);

			yield return null;
		}

		yield return new WaitForSeconds (3f); // Wait for n seconds

		while(transform.localScale != minSize) { // Then hide the boss' call
			transform.localScale = Vector3.MoveTowards(transform.localScale, minSize, animationSpeed * Time.deltaTime);
			transform.position = Vector3.MoveTowards (transform.position, oldPosition, movementSpeed * Time.deltaTime);
			
			yield return null;
		}

		yield return null;
	}
}