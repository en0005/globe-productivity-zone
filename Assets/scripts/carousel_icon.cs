﻿using UnityEngine;
using System.Collections;

public class carousel_icon : MonoBehaviour {
	public int 			currentPosition;
	public float 		transitionSpeed = 0.06f;
	
	private Transform 			targetToFollow;
	private Vector3				targetScale;
	private Vector3				tempPosition;
	private Vector3[] 			scaleList  = new Vector3[15];
	private Vector3[] 			offsetList = new Vector3[15];
	private scroll_v3 			carouselScript;
	private Shader 				objectShader;

	void Start() {
		setScaleList ();
		setOffsetList ();

		// Automatically find the target to follow if not set
		string pointName = transform.name.Replace ("icon", "point");
		if (GameObject.Find (pointName) != null) {
			targetToFollow = GameObject.Find (pointName).transform;
		}

		if(currentPosition == 7) {
			renderer.material.SetFloat ("_Blend", 0f);
		} else {
			renderer.material.SetFloat("_Blend", 1f);
		}

		tempPosition.x 	= targetToFollow.position.x + offsetList[currentPosition].x;
		tempPosition.y 	= targetToFollow.position.y - offsetList[currentPosition].y;
		tempPosition.z 	= offsetList[currentPosition].z;
		transform.localPosition = Vector3.MoveTowards(transform.localPosition , tempPosition, 500 + Time.deltaTime);

		transform.localScale = Vector3.MoveTowards (transform.localScale, scaleList [currentPosition], 500 + Time.deltaTime);
		carouselScript = GameObject.Find ("carousel").GetComponent<scroll_v3> ();
	}

	public void carouselRotated(scroll_v3.Direction carouselSpinDir) {
		StartCoroutine("scaleIcon");
		StartCoroutine("moveIcon");
	}

	IEnumerator moveIcon() {
		tempPosition.x 	= targetToFollow.position.x + offsetList[currentPosition].x;
		tempPosition.y 	= targetToFollow.position.y - offsetList[currentPosition].y;
		tempPosition.z 	= offsetList[currentPosition].z;

		while(transform.position != tempPosition) {
			transform.localPosition = Vector3.Lerp(transform.position , tempPosition, transitionSpeed + Time.deltaTime);
			yield return null;
		}

		carouselScript.setRotateDone(true);
		yield return null;
	}

	IEnumerator scaleIcon() {
		scroll_v3.Direction spinDir = carouselScript.spinDirection ();
		GameObject background = GameObject.Find ("background");

		if (spinDir == scroll_v3.Direction.left) {
			this.currentPosition += 1;
		} else if (spinDir == scroll_v3.Direction.right) {
			this.currentPosition -= 1;
		}
		
		while(transform.localScale != scaleList[currentPosition]) {	
			transform.localScale = Vector3.Slerp(transform.localScale , scaleList[currentPosition], transitionSpeed + Time.deltaTime);

			float newBlendValue;
			// Blend icon materials
			if(this.currentPosition == 7) {
				newBlendValue = Mathf.Lerp (renderer.material.GetFloat("_Blend"), 0f, transitionSpeed + Time.deltaTime);
			} else {
				newBlendValue = Mathf.Lerp (renderer.material.GetFloat("_Blend"), 1f, transitionSpeed + Time.deltaTime);
			}
			renderer.material.SetFloat ("_Blend", newBlendValue);
			
			yield return null;
		}

		yield return null;
	}

	void setScaleList() {
		scaleList[0]   = new Vector3(0.8f, 		0.8f, 		0f);
		scaleList[1]   = new Vector3(1f, 		1f, 		0f);
		scaleList[2]   = new Vector3(1.5f, 		1.5f, 		0f);
		scaleList[3]   = new Vector3(2f, 		2f, 		0f);
		scaleList[4]   = new Vector3(2.5f, 		2.5f, 		0f);
		scaleList[5]   = new Vector3(4f, 		4f, 		0f);
		scaleList[6]   = new Vector3(6f, 		6f, 		0f);
		scaleList[7]   = new Vector3(10f, 		10f, 		0f);
		scaleList[8]   = scaleList[6]; // Values are referenced for faster adjustment of sizes
		scaleList[9]   = scaleList[5];
		scaleList[10]  = scaleList[4];
		scaleList[11]  = scaleList[3];
		scaleList[12]  = scaleList[2];
		scaleList[13]  = scaleList[1];
		scaleList[14]  = scaleList[0];
	}

	void setOffsetList() {
		offsetList[0]   = new Vector3(-2.5f, 	0.1f, 		-0.25f);
		offsetList[1]   = new Vector3(0.75f, 	0.2f, 		-0.5f);
		offsetList[2]   = new Vector3(2.5f,	    0.1f, 		-0.75f);
		offsetList[3]   = new Vector3(2.5f, 	0.4f, 		-1f);
		offsetList[4]   = new Vector3(0.4f, 	0.6f, 		-1.25f);
		offsetList[5]   = new Vector3(0.5f, 	1f, 		-1.50f);
		offsetList[6]   = new Vector3(0.0f, 	1f, 		-1.75f);
		offsetList[7]   = new Vector3(0f, 		0f, 		-2f);
		offsetList[8]   = new Vector3(-offsetList[6].x, offsetList[6].y,  offsetList[6].z); // Values are referenced for faster adjustment of sizes
		offsetList[9]   = new Vector3(-offsetList[5].x, 0.2f,             offsetList[5].z);
		offsetList[10]  = new Vector3(-offsetList[4].x, -0.6f, 			  offsetList[4].z);
		offsetList[11]  = new Vector3(-offsetList[3].x, -0.4f, 			  offsetList[3].z);
		offsetList[12]  = new Vector3(-offsetList[2].x, -0.3f,  		  offsetList[2].z);
		offsetList[13]  = new Vector3(-offsetList[1].x, -0.2f, 			  offsetList[1].z);
		offsetList[14]  = new Vector3(-offsetList[0].x, -0.1f, 			  offsetList[0].z);

	}
}
