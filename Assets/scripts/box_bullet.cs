﻿using UnityEngine;
using System.Collections;

public class box_bullet : MonoBehaviour {
	bool flying = false;
	bool isActive = true;
	float appliedForce = 0f;
	float spin1;
	float spin2;
	cube_generator generator;

	// Use this for initialization
	void Start () {
		generator = GameObject.Find ("CubeGenerator").GetComponent<cube_generator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (flying == true) {
			Vector3 targetDir = GameObject.Find ("Sphere").transform.position - transform.position;
			targetDir.Normalize();

			rigidbody.AddForce (targetDir * this.appliedForce);
			transform.Rotate (Vector3.up * spin1 * Time.deltaTime);
			transform.Rotate (Vector3.right * spin2 * Time.deltaTime);
		}

		if(this.isActive == true && this.flying == true) {
			if(transform.position.y < -8) {
				this.isActive = false;
				StartCoroutine("destroyCube");
				generator.generateCube();
			} else if(transform.position.x < -5 || transform.position.y > 5) {
				this.isActive = false;
				StartCoroutine("destroyCube");
				generator.generateCube();
			}
		}
			
	}

	public void launchBox(float force) {
		if(this.isActive == true) {
			rigidbody.useGravity = true;
			spin1 = Random.Range(-360, 360);
			spin2 = Random.Range(-450, 450);

			flying = true;
			transform.Rotate(Vector3.up * Random.Range(-270, 270) * 1000);
			rigidbody.AddForce (Vector3.up * 700);
			this.appliedForce = force;
		}
	}

	void OnCollisionEnter(Collision collision) {
		if(collision.collider.name == "hollow_box_mid" || collision.collider.name == "hollow_box_left" || collision.collider.name == "hollow_box_right" || collision.collider.name == "box_bullet_prefab") {
			if(flying == true) {
				flying = false;
				this.isActive = false;
				print ("force is turned off");
				StartCoroutine("destroyCube");
				generator.generateCube();
			}
		}
	}

	IEnumerator destroyCube() {
		yield return new WaitForSeconds(2f);
		Destroy (gameObject);
	}
}
