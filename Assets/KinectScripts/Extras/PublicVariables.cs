﻿using UnityEngine;
using System.Collections;

public class PublicVariables : MonoBehaviour {
    private static PublicVariables instance;
    private bool IsSceneReady = false;
    private bool IsPersonDetected = false;
    private bool IsWaveDetected = false;
    private bool IsInteractionReady = false;


    public static PublicVariables Instance
    {
        get
        {
            return instance;
        }
    }

    public bool checkWaveDetected()
    {
        return IsWaveDetected;
    }

    public void SetcheckWaveDetected(bool tmpBool)
    {
        IsWaveDetected = tmpBool;
    }

    public bool checkPersonDetected()
    {
        return IsPersonDetected;
    }

    public void SetPersonDetected(bool tmpBool)
    {
        IsPersonDetected = tmpBool;
    }

    public bool checkIsSceneReady()
    {
        return IsSceneReady;
    }

    public void SetIsSceneReady(bool tmpBool)
    {
        IsSceneReady = tmpBool;
    }


    public bool checkIsInteractionReady()
    {
        return IsInteractionReady;
    }

    public void SetIsInteractionReady(bool tmpBool)
    {
        IsInteractionReady = tmpBool;
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
