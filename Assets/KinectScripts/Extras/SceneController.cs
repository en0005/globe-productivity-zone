﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class SceneController : MonoBehaviour
{
    private KinectManager Kmanager;
    private InteractionManager Imanager;
    private GestureListener gestureListener;
    public GUIText GestureInfo;

    float 
        Idle_Timer = 0.0f,
        Gesture_timer = 0.0f;

    bool 
        DectectedPSI = false,
        HandsDown = false;

    Texture2D tmpUserMap;

    IEnumerator Start()
    {
        Resources.UnloadUnusedAssets();
        while (Kmanager == null || Imanager == null)
        {
            if (Kmanager == null)
                Kmanager = KinectManager.Instance;
            if (Imanager == null)
                Imanager = InteractionManager.Instance;
        }

        appController.CMS_Started = true;
        // Disable the user map once game starts
        if (Application.loadedLevel == 2)
        {
            Kmanager.ComputeUserMap = false;
            Kmanager.DisplayUserMap = false;
        }
        else
        {
            Kmanager.ComputeUserMap = true;
            Kmanager.DisplayUserMap = true;
        }

        yield return null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (!appController.CMS_Started)
            //appController.Instance.StartCMS();
        //if (appController.AppShown == appController.AppList.SPLASH)
        //    appController.Instance.Send("topSplash");

        //if (appController.AppShown == appController.AppList.ZONE)
            //appController.Instance.Send("keepZoneOnTop");

        if ((Kmanager != null) && (Imanager != null) && Imanager.IsInteractionInited())
        {
            if (Kmanager.IsUserDetected())
            {
                //appController.Instance.Send("is player in: " + Imanager.isPlayerIN().ToString());
                // TO DO SEND SIGNAL TO OTHER PROGRAM IF USER IS DETECTED
                if (!Imanager.isPlayerIN())
                {
                    StartCoroutine(ShowSplash());

                    Kmanager.DetectClosestUser = true;

                    // Write the image file only if player is detected and when the zone is not active
                    if (appController.AppShown != appController.AppList.ZONE)
                    {
                        try
                        {
                            tmpUserMap = Kmanager.GetUsersLblTex();
                            byte[] bytes = tmpUserMap.EncodeToPNG();
                            // Create a file using the FileStream class.
                            if (!Directory.Exists("C:\\HTech\\KinectTmpFile"))
                                Directory.CreateDirectory("C:\\HTech\\KinectTmpFile");

                            FileStream fWrite = new FileStream("C:\\HTech\\KinectTmpFile\\UserColorMap.png", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite, 8, true);

                            // Write the bytes to the file.
                            fWrite.Write(bytes, 0, bytes.Length);

                            // Close the stream.
                            fWrite.Close();
                        }
                        catch (Exception ex)
                        {
                            Debug.Log(ex.Message);
                        }
                    }
                }

                Idle_Timer = 0;
                gestureListener = Kmanager.GetComponent<GestureListener>();
                Imanager.SetPlayerDetected(true);


                Vector3 ShoulderPos = Kmanager.GetJointPosition(Imanager.GetUserID(), (int)KinectWrapper.SkeletonJoint.RIGHT_SHOULDER);
                Vector3 HandRightPos = Kmanager.GetJointPosition(Imanager.GetUserID(), (int)KinectWrapper.SkeletonJoint.RIGHT_HAND);
                Vector3 HandLeftPos = Kmanager.GetJointPosition(Imanager.GetUserID(), (int)KinectWrapper.SkeletonJoint.LEFT_HAND);
                DectectedPSI = false;
                if (HandRightPos.y > ShoulderPos.y && HandLeftPos.y > ShoulderPos.y)
                {
                    //if (!DectectedPSI)
                    Gesture_timer += Time.deltaTime;

                    if (Gesture_timer > 1.25f)
                    {
                        DectectedPSI = true;
                        Gesture_timer = 0;
                    }
                }
                else
                {
                    Gesture_timer = 0;
                    HandsDown = true;
                }
                GestureInfo.guiText.text = Kmanager.GetLastPlayerID().ToString() + "  " + Imanager.GetUserID().ToString();
                if (DectectedPSI && !Imanager.isPlayerIN() && Kmanager.GetLastPlayerID() != Imanager.GetUserID())
                {
                    ShowZone();
                    Kmanager.DetectClosestUser = false;
                    Imanager.SetDisableCursor(false);
                    Imanager.SetSceneReady(true);
                    Imanager.SetResetScene(true);
                    Imanager.SetPlayerIN(true);
                    HandsDown = false;
                }
                else if ((DectectedPSI && Imanager.isPlayerIN() && HandsDown) || Imanager.isplayerLogout())
                {
                    HideSplash();
                    appController.AppShown = appController.AppList.ZONE;
                    ShowCMS();

                    //GestureInfo.guiText.text = "PLAYERS LOGOUT!";
                    Kmanager.SetLastPlayerID((int)Imanager.GetUserID());
                    //Kmanager.RemoveUser(Imanager.GetUserID());
                    Imanager.SetResetScene(false);
                    Imanager.SetSceneReady(false);
                    Imanager.SetPlayerDetected(false);
                    Imanager.SetPlayerIN(false);
                    Imanager.SetPlayerLogout(false);
                    Idle_Timer = 0;
                    PlayerPrefs.SetInt("prodPlayAgain", 0); // Enables the splash screen on load
                    Application.LoadLevel(Level.MECHANICS);

                }
            }
            else
            {
                HideSplash();
                
                if (Imanager.isResetScene())
                {
                    Idle_Timer += Time.deltaTime;
                    if (Idle_Timer > 5.0f)
                    {
                        ExitGame();
                    }
                }
            }
        }


        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Kmanager.SensorAngle += 2;
            KinectWrapper.NuiCameraElevationSetAngle(Kmanager.SensorAngle);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Kmanager.SensorAngle -= 2;
            KinectWrapper.NuiCameraElevationSetAngle(Kmanager.SensorAngle);
        }
    }

    public void ExitGame()
    {
        appController.AppShown = appController.AppList.ZONE;
        ShowCMS();

        Imanager.SetResetScene(false);
        Imanager.SetSceneReady(false);
        Imanager.SetPlayerDetected(false);
        Imanager.SetPlayerIN(false);
        Imanager.SetPlayerLogout(true);
        Idle_Timer = 0;
        PlayerPrefs.SetInt("prodPlayAgain", 0); // Enables the splash screen on load
        Application.LoadLevel(Level.MECHANICS);
    }

    IEnumerator ShowSplash()
    {
        if (!appController.Instance.gameStarted && appController.AppShown != appController.AppList.SPLASH
                && appController.AppShown != appController.AppList.ZONE)
        {
            appController.AppShown = appController.AppList.SPLASH;

            yield return new WaitForSeconds(1f);
            appController.Instance.Send("playerDetected");
        }
    }

    void HideSplash()
    {
        //Imanager.SetPlayerIN(false);
        if (appController.AppShown != appController.AppList.CMS)
        {
            appController.Instance.Send("noPlayer");
            appController.AppShown = appController.AppList.CMS;
        }
    }

    void ShowZone()
    {
        if (appController.AppShown != appController.AppList.ZONE)
        {
            appController.Instance.gameStarted = true;
            appController.Instance.Send("noPlayer");
            appController.Instance.Send("showZone");
            appController.AppShown = appController.AppList.ZONE;
        }
    }

    public void ShowCMS()
    {
        if (appController.AppShown != appController.AppList.CMS)
        {
            appController.Instance.gameStarted = false;
            appController.Instance.Send("showCMS");
            appController.AppShown = appController.AppList.CMS;
        }
    }
}

public class Level
{
    public const short
        MECHANICS = 1,
        GAME = 2,
        GAME_OVER = 3;
}
