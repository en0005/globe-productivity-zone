﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;
using System.IO;
using System.Collections.Generic;


public class CursorInteraction : MonoBehaviour
{
    public float dragSpeed = 3.0f;
    public GameObject[] ClickableObjects;
    public AudioClip[] audioClip;
    public GameObject DebugGUI;
    public float Click_Sensitive = 0.4f;
    public float distance = -1;
    public float goDepth = -7;

    public Material[] Intromaterials;

    private InteractionManager manager;
    private KinectManager Kmanager;
    private GameObject splashObject;

    // GUI-Texture object to be used as screen cursor
    private GameObject handCursor;
    private bool isLeftHandDrag;
    private Vector3 OrigScale;
    private Queue<double> _weightedX = new Queue<double>();
    private Queue<double> _weightedY = new Queue<double>();

    bool ObjisHit = false;
    GameObject hitObject;
    GameObject IntroScreen;
    Animator anim;
    float clickratio = 0f;
    float clickratiofactor = 7f;
    bool isPressed;
    private scroll_v3 Scroll;
    int EventList;
    string EName;
    Vector3 hitPointPos;
    float SaveFirstDetectedHandPosZ;
    float FirstHandposZ; // This the variable to hold the actual z pos after we change it to -8.5f 
    float x_pos;
    float shoulder_hand_diff1;
    float shoulder_hand_diff2;
    float HandposDiffZ;
    bool handIsPressed;
    bool click_once = false;
    float startTime;
    float endTime;
    float swipeThreshold = 0.15f;
    bool icon_hit;
    float Idle_Timer = 0.0f;

    private List<Vector3> ClickableObjsVectors;
    int Hitctr = 0;
    Vector3 v3ViewPort;
    Vector3 v3BottomLeft;
    Vector3 v3TopRight;
    bool isgrip = false;
    bool isVideoShown = false;
    bool isVideoplaying = false;
    Vector3 newCursorPos = Vector3.zero;
    GameObject modalDim;

    prod_buttons xButton;
    tagline_script TaglineScript;
    bool isclicking = false;
    float ClickTimer1;
    float ClickTimer2;
    float ClickTimeDiff;
    bool ClickandTime = false;

    void Awake()
    {
        manager = InteractionManager.Instance;
        Kmanager = KinectManager.Instance;
        Application.targetFrameRate = 50;
    }

    void Start()
    {
        handCursor = GameObject.Find("handcursor");
        anim = handCursor.GetComponent<Animator>();
        //Scroll = GameObject.Find("carousel").GetComponent<scroll_v3>();
        splashObject = GameObject.Find("music_zone");

        ClickableObjsVectors = new List<Vector3>();
        foreach (GameObject obj in ClickableObjects)
            ClickableObjsVectors.Add(obj.transform.localScale);
    }

    void Update()
    {
        //if (manager != null && manager.IsInteractionInited() && !manager.isDisableCursor())
        if (manager.IsSceneReady())
        {

            //if (Kmanager == null)
            //    Kmanager = KinectManager.Instance;

            //if (Kmanager.IsUserDetected())
            //{
                
                Idle_Timer = 0;
                Vector3 screenNormalPos = Vector3.zero;
                Vector3 screenPixelPos = Vector3.zero;
                screenNormalPos = isLeftHandDrag ? manager.GetLeftHandScreenPos() : manager.GetRightHandScreenPos();
                //v1 = screenNormalPos;
                FirstHandposZ = screenNormalPos.z;
                screenNormalPos.z = -2.5f;
                // convert the normalized screen pos to 3D-world pos
                screenPixelPos.x = (int)(screenNormalPos.x * Camera.main.pixelWidth);
                screenPixelPos.y = (int)(screenNormalPos.y * Camera.main.pixelHeight);
                // World Cursor Position
                 newCursorPos = Camera.main.ScreenToWorldPoint(screenPixelPos);
                newCursorPos.z = -2.5f;
                // Smoothing moving linear average
                newCursorPos = ExponentialWeightedAvg(newCursorPos);
                newCursorPos = Vector3.Lerp(handCursor.transform.position, newCursorPos, dragSpeed * Time.deltaTime);


                if (newCursorPos != Vector3.zero)
                {
                    // Check for Collision
                    //     Hitctr = 0;
                    //foreach (GameObject Iobj in ClickableObjects)
                    //{
                    //    if ((EName == Iobj.name) && (Iobj.name.IndexOf("icon") < 0))
                    //        Iobj.transform.localScale = ClickableObjsVectors[Hitctr];
                    //    Hitctr++;
                    //}

                    Ray rayCenter = Camera.main.ScreenPointToRay(screenPixelPos);
                    RaycastHit hit;

                    if (xButton != null)
                        xButton.shrinkDown();
                    // Revised
                   ClickTimeDiff = 0;

                    if (Physics.Raycast(rayCenter, out hit))
                    {

                        
                            isclicking = false;

                       
                        if (hit.collider.name == "next" || hit.collider.name == "skip" || hit.collider.name == "play" || hit.collider.name == "modaldimNext" || hit.collider.name == "play_again" || hit.collider.name == "exit")
                        {

                            EName = hit.collider.name;
                            GameObject obj = hit.collider.gameObject;
                            xButton = obj.GetComponent<prod_buttons>();
                            //modalDim = obj.transform.parent.Find(hit.collider.name).gameObject;
                            //xButton = obj.transform.parent.Find(hit.collider.name).gameObject.GetComponent<prod_buttons>();
                            xButton.sizeUp();

                            hitPointPos = obj.renderer.bounds.center;
                            hitPointPos.z = -2.5f;
                           
                            hitPointPos = hit.collider.gameObject.transform.position;
                            //if (obj.name.IndexOf("icon") < 0)
                            //        obj.transform.localScale *= 1.2f;

                            float tmpshoulder_hand_diff = isLeftHandDrag ? JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft)) : JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight));

                            if (tmpshoulder_hand_diff < shoulder_hand_diff1)
                                shoulder_hand_diff1 = tmpshoulder_hand_diff;

                            if (FirstHandposZ < SaveFirstDetectedHandPosZ)
                                SaveFirstDetectedHandPosZ = FirstHandposZ;


                            
                            
                            if (!ObjisHit)
                            {
                                ClickTimer1 = Time.time;
                                icon_hit = false;
                                shoulder_hand_diff1 = isLeftHandDrag ? JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft)) : JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight));
                                SaveFirstDetectedHandPosZ = FirstHandposZ;
                                //anim.Play("RightHandHover");
                                hitObject = obj;

                                if (obj.name.IndexOf("icon") >= 0)
                                {
                                    icon_hit = true;
                                    isgrip = false;
                                }
                                click_once = false;
                                ObjisHit = true;
                            }
                            //else


                            ClickTimer2 = Time.time;

                            ClickTimeDiff = ClickTimer2 - ClickTimer1;

                        }
                        else if (hit.collider.name == "paper" || hit.collider.name =="modal_dialog")
                        {
                            ObjisHit = false;

                            EName = hit.collider.name;
                            GameObject obj = hit.collider.gameObject;
                            modalDim = obj.transform.parent.Find(hit.collider.name).gameObject;
                            hitPointPos = hit.collider.gameObject.transform.position;
                            if (obj.transform.parent.Find("modaldimNext") != null)
                            {
                                EName = "modaldimNext";
                                //DebugGUI.guiText.text = "Modal Next";
                            }
                            else if (obj.transform.parent.Find("next") != null)
                            {
                                xButton = obj.transform.parent.Find("next").gameObject.GetComponent<prod_buttons>();
                                EName = "next";
                            }
                            else if (obj.transform.parent.Find("play") != null)
                            {
                                xButton = obj.transform.parent.Find("play").gameObject.GetComponent<prod_buttons>();
                                EName = "next";
                            }
                            icon_hit = true;
                        }

                        // 2 Type of checking if press was made
                        // 1.) get the before and after z poistion of the hands to check if press was made
                        HandposDiffZ = FirstHandposZ - SaveFirstDetectedHandPosZ;
                        // 2.) Get the joint distance of hands vs shoulder to check if press was made
                        shoulder_hand_diff2 = isLeftHandDrag ? JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft)) : JointDistance(Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight), Kmanager.GetJointPosition(manager.GetUserID(), (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight));
                        //if (HandposDiffZ > 0.1f)
                        //    isclicking = true;
                        //Vector3 v3 = (v2 - v1).magnitude;


                    }
                    else
                    {

                        if (ObjisHit)
                        {

                            ObjisHit = false;
                            click_once = false;
                        }

                    }
                }
                else
                {
                    isPressed = false;
                    //newCursorPos = Vector3.zero;
                }
                // Revised
                ClickandTime = false;
                if (ClickTimeDiff >= 1.5f)
                    ClickandTime = true;
                DebugGUI.guiText.text = " DIFF : " + HandposDiffZ.ToString();
                if (manager.IsLeftHandPrimary())
                {
                    //  Idle_Timer = 0;
                    isLeftHandDrag = true;
                    // if the left hand is primary, check for left hand grip
                    if (manager.GetLastLeftHandEvent() == InteractionWrapper.InteractionHandEventType.Grip)
                    {
                        //DebugGUI.guiText.text = "X Pos : " + (x_pos - screenNormalPos.x).ToString();
                        if ((icon_hit) && (ObjisHit))
                        {
                            startTime = Time.time;
                            anim.Play("LeftHandGrip");
                            isPressed = false;

                            if (!isgrip)
                            {
                                x_pos = screenNormalPos.x;
                                isgrip = true;
                            }

                            if (x_pos - screenNormalPos.x > 0.3f)
                            { // Swipe left

                                DoEvents("arrow_right", hitPointPos);
                                isgrip = false;
                            }
                            else if (x_pos - screenNormalPos.x < -0.3f)
                            { // Swipe right

                                DoEvents("arrow_left", hitPointPos);
                                isgrip = false;
                            }


                        }
                        else
                        {
                            isgrip = false;
                            isPressed = false;
                            anim.Play("LeftHandIdle");
                        }
                    }
                    // Detect Click gesture . Get all possible gesture.
                    //else if (((((HandposDiffZ >= Click_Sensitive) || ((shoulder_hand_diff2 - shoulder_hand_diff1) >= 0.03f)) && ClickTimeDiff > 0.5f) || ClickandTime) && (!icon_hit))
                    else if (((HandposDiffZ >= Click_Sensitive) || ((shoulder_hand_diff2 - shoulder_hand_diff1) >= 0.08f)) && (!icon_hit))
                    {

                        isgrip = false;
                        if (ObjisHit)
                        {
                            //ObjisHit = false;
                            if (!click_once)
                            {
                                isPressed = true;
                                handCursor.transform.position = hitObject.renderer.bounds.center;
                                DoEvents(EName, hitPointPos);
                                anim.Play("LeftHandClick");
                                click_once = true;
                                //click_once = true;
                                //anim.Play("RightHandClick");
                                //DebugGUI.guiText.text = "Collider : " + EName;
                                //isPressed = true;                                                                                         
                                //ClickandTime = false;
                            }
                            else
                            {
                                if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0) && click_once)
                                {
                                    //Debug.Log("Done With animation");
                                    anim.Play("LeftHandIdle");
                                    isPressed = false;
                                    if (ObjisHit)
                                    {

                                        ObjisHit = false;
                                        click_once = false;
                                    }
                                    //DoEvents(EName, hitPointPos);
                                    //anim.Play("RightHandIdle");
                                    //isPressed = false;
                                    //HandposDiffZ = 0;
                                    //ObjisHit = false;
                                    //click_once = false;

                                }
                            }
                        }
                        else
                        {
                            if (ObjisHit)
                            {
                                anim.Play("LeftHandIdle");
                                DebugGUI.guiText.text = "HOVER : ";
                            }
                        }
                        //else
                        //{
                        //    //if (!isPressed)
                        //    //{
                        //        isPressed = false;
                        //        anim.Play("RightHandIdle");
                        //    //}
                        //}

                    }
                    else
                    {
                        if (!ObjisHit)
                        {
                            isPressed = false;
                            anim.Play("LeftHandIdle");
                        }
                        else
                            anim.Play("LeftHandIdle");
                        isgrip = false;

                    }
                }
                else if (manager.IsRightHandPrimary())
                {

                    //  Idle_Timer = 0;
                    isLeftHandDrag = false;
                    // if the right hand is primary, check for right hand grip
                    if (manager.GetLastRightHandEvent() == InteractionWrapper.InteractionHandEventType.Grip)
                    {
                        DebugGUI.guiText.text = "GRIP";
                        if (icon_hit)
                        {
                            startTime = Time.time;
                            anim.Play("RightHandGrip");
                            isPressed = false;

                            if (!isgrip)
                            {
                                x_pos = screenNormalPos.x;
                                isgrip = true;
                            }

                            if (x_pos - screenNormalPos.x > 0.3f)
                            { // Swipe left
                                DoEvents(EName, hitPointPos);
                                isgrip = false;
                            }

                        }
                        //else
                        //{
                        //    isgrip = false;
                        //    isPressed = false;
                        //    anim.Play("RightHandIdle");
                        //}
                    }
                    //else if (((((HandposDiffZ >= Click_Sensitive) || ((shoulder_hand_diff2 - shoulder_hand_diff1) >= 0.03f)) && ClickTimeDiff > 0.5f) || ClickandTime) && (!icon_hit))
                    else if (((HandposDiffZ >= Click_Sensitive) || ((shoulder_hand_diff2 - shoulder_hand_diff1) >= 0.08f)) && (!icon_hit))
                    {
                        
                        isgrip = false;
                        if (ObjisHit)
                        {
                            //ObjisHit = false;
                            if (!click_once)
                            {
                                isPressed = true;
                                handCursor.transform.position = hitObject.renderer.bounds.center;
                                DoEvents(EName, hitPointPos);
                                anim.Play("RightHandClick");
                                click_once = true;
                                //click_once = true;
                                //anim.Play("RightHandClick");
                                //DebugGUI.guiText.text = "Collider : " + EName;
                                //isPressed = true;                                                                                         
                                //ClickandTime = false;
                            }
                            else
                            {
                                if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0) && click_once)
                                {
                                    //Debug.Log("Done With animation");
                                    anim.Play("RightHandIdle");
                                    isPressed = false;
                                    if (ObjisHit)
                                    {

                                        ObjisHit = false;
                                        click_once = false;
                                    }
                                    //DoEvents(EName, hitPointPos);
                                    //anim.Play("RightHandIdle");
                                    //isPressed = false;
                                    //HandposDiffZ = 0;
                                    //ObjisHit = false;
                                    //click_once = false;

                                }
                            }
                        }
                        else
                        {
                            if (ObjisHit)
                            {
                                anim.Play("RightHandIdle");
                                DebugGUI.guiText.text = "HOVER : ";
                            }
                        }
                        //else
                        //{
                        //    //if (!isPressed)
                        //    //{
                        //        isPressed = false;
                        //        anim.Play("RightHandIdle");
                        //    //}
                        //}

                    }
                    else
                    {
                        if (!ObjisHit)
                        {
                        isPressed = false;
                        anim.Play("RightHandIdle");
                        }
                        else
                            anim.Play("RightHandIdle");
                        isgrip = false;

                    }


                }
                else
                {
                    //Reset everything to original size
                    isgrip = false;
                    Hitctr = 0;
                    foreach (GameObject obj in ClickableObjects)
                    {
                        if ((EName == obj.name) && (obj.name.IndexOf("icon") < 0))
                            obj.transform.localScale = ClickableObjsVectors[Hitctr];
                        Hitctr++;
                    }
                    anim.Play("RightHandIdle");
                    isPressed = false;
         
                }

                if (!isPressed)
                    handCursor.transform.position = newCursorPos;

        }
        else
        {
            if (manager == null)
            manager = InteractionManager.Instance;

        }
    }

    //IEnumerator nextPage()
    //{
    //    while (transform.parent.position != leftPoint)
    //    {
    //        transform.parent.position = Vector3.Lerp(transform.parent.position, leftPoint, 0.1f + Time.deltaTime);

    //        if ((transform.parent.position - leftPoint).magnitude < 0.05f)
    //            transform.parent.position = leftPoint;

    //        yield return null;
    //    }

    //    Destroy(transform.parent.gameObject);
    //    yield return null;
    //}

    IEnumerator Reversesplash()
    {
        float newBlendValue;

        //splashTextObject.renderer.enabled = false;
        // Change alpha
        splashObject.renderer.sharedMaterial = Intromaterials[0];
        while (splashObject.renderer.material.GetFloat("_Blend") != 0.01f)
        {

            newBlendValue = Mathf.Lerp(splashObject.renderer.material.GetFloat("_Blend"), 0.01f, 0.01f + Time.deltaTime);

            splashObject.renderer.material.SetFloat("_Blend", newBlendValue);
            //globeObject.renderer.material.SetFloat ("_Blend", newBlendValue);

            if (newBlendValue <= 0.1f)
            {
                splashObject.renderer.material.SetFloat("_Blend", 0.01f);
                //globeObject.renderer.material.SetFloat("_Blend", 1f);
                break;
            }

            yield return null;
        }



        yield return null;
    }

    private double ExponentialMovingAverage(double[] data, double baseValue)
    {
        double numerator = 0;
        double denominator = 0;

        double average = 0;
        for (int i = 0; i < data.Length; i++)
            average += data[i];

        average /= data.Length;

        for (int i = 0; i < data.Length; ++i)
        {
            numerator += data[i] * Math.Pow(baseValue, data.Length - i - 1);
            denominator += Math.Pow(baseValue, data.Length - i - 1);
        }

        numerator += average * Math.Pow(baseValue, data.Length);
        denominator += Math.Pow(baseValue, data.Length);

        return numerator / denominator;
    }

    private Vector3 ExponentialWeightedAvg(Vector3 joint)
    {
        _weightedX.Enqueue(joint.x);
        _weightedY.Enqueue(joint.y);

        if (_weightedX.Count > 5)
        {
            _weightedX.Dequeue();
            _weightedY.Dequeue();
        }

        double x = ExponentialMovingAverage(_weightedX.ToArray(), 0.9);
        double y = ExponentialMovingAverage(_weightedY.ToArray(), 0.9);

        Vector3 _return = Vector2.zero;
        _return.x = (float)x;
        _return.y = (float)y;
        _return.z = -2.5f;

        return _return;
    }

    private float JointDistance(Vector3 joint1, Vector3 joint2)
    {
        float dX = joint1.x - joint2.x;
        float dY = joint1.y - joint2.y;
        float dZ = joint1.z - joint2.z;
        return (float)Math.Sqrt((dX * dX) + (dY * dY) + (dZ * dZ));
    }

    void DoEvents(string EventName, Vector3 ObjHitPos)
    {
        foreach (prod_buttons btn in GameObject.FindObjectsOfType<prod_buttons>())
        {
            if (btn.transform.parent == null)
            {
                btn.collider.enabled = false;
                if (btn.parentObject != null)
                btn.transform.parent = btn.parentObject.transform;
            }
        }

        switch (EventName)
        {
            case "next":
                xButton.nextSlide();
                break;
            case "skip":
                xButton.skipMechanics();
                break;
            case "play":
                xButton.nextSlide();
                newCursorPos.z = -2.5f;
                handCursor.transform.position = newCursorPos;
                break;
            case "modaldimNext":
                TaglineScript = modalDim.GetComponentInParent<tagline_script>();
                TaglineScript.moveLeft();
                break;
            case "play_again":
                xButton.playAgain();
                break;
            case "exit":
                //manager.SetSceneReady(false);
                //manager.SetPlayerDetected(false);
                //Kmanager.SetLastPlayerID((int)manager.GetUserID());
                //manager.SetPlayerIN(false);
                //manager.SetPlayerLogout(true);
                xButton.exitGame();
                break;
            default:
                break;

        }
    }


}
